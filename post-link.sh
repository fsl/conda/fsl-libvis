if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper create_lut fsl_boxplot fsl_histogram fsl_tsplot overlay pngappend slicer wpng
fi
